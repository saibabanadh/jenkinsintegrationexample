const express = require('express');
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));


app.get('/', (req,res) => {
	res.status(200).json({message:"Welcome to Jenkins Build"})
});

app.get('/ping', (req,res) => {
	res.status(200).json({message:"PONG"});
});

app.listen(9000, ()=>{
	console.log("Admin API is running on port:", 9000)
});